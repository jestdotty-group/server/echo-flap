const fs = require('fs').promises
const http = require('http')

const socketIO = require('socket.io')
const Koa = require('koa')
const Router = require('@koa/router')
const log = require('tracer').colorConsole({
	format: [
		"{{timestamp}} [{{title}}] {{file}}:{{line}} {{message}}",
		{log:"{{timestamp}} {{message}}"}
	],
	dateformat: "HH:MM:ss.L",
})

const app = new Koa()
app.context.log = log
const router = new Router()

//setup
process.on('uncaughtException', log.error)
app.on('error', (err, ctx)=> log.error(ctx? '*': '', err))
app.use(async (ctx, next)=>{
	ctx.set('Access-Control-Allow-Origin', process.env.CLIENT || ctx.get('Origin'))
	ctx.set('Access-Control-Allow-Headers', 'Content-Type, Accept')

	const start = Date.now()
	try{ await next() }catch(err){
		ctx.status = err.status || 500
		ctx.body = err.message
		ctx.app.emit('error', err, ctx)
	}
	ctx.set('X-Response-Time', Date.now() - start)

	;(ctx.status < 400? console.log: console.warn)( //snoop snoop
		`${ctx.status} ${ctx.method} ${ctx.response.get('X-Response-Time')}ms ${ctx.url} ${ctx.ip} ${process.env.CLIENT || `*${ctx.get('Origin')}`}`
	)
})

fs.readdir('./routes').then(files=>{
	//routes
	for(const file of files){
		const module = require(`./routes/${file}`)
		if(module instanceof Router) router.use(module.routes(), module.allowedMethods())
		else router.use(module.name || '/'+name, module.router.routes(), module.router.allowedMethods())
	}
	app.use(router.routes())
	app.use(router.allowedMethods())

	//sockets
	const server = http.createServer(app.callback())
	const io = socketIO(server,{
		path: '/echo',
		cors: {origin: (origin, cb)=> cb(null, process.env.CLIENT || origin)}
	})
	io.on('connection', socket=>{
		console.log(`connected ${socket.id}`)
		socket.on('join', name=>{
			socket.name = name
			console.log(`joined ${socket.id}:${socket.name}`)
			socket.broadcast.emit('join', {name: socket.name})
		})
		socket.on('disconnect', ()=>{
			console.log(`disconnected ${socket.id}:${socket.name}`)
			socket.broadcast.emit('leave', {name: socket.name})
		})
		socket.on('rename', name=>{
			console.log(`renaming ${socket.id}:${socket.name} to ${name}`)
			socket.broadcast.emit('rename', {name: socket.name, rename: name})
			socket.name = name
		})
		socket.on('typing', ()=> socket.broadcast.emit('typing', {id: socket.id, name: socket.name}))
		socket.on('stop typing', ()=> socket.broadcast.emit('stop typing', {id: socket.id, name: socket.name}))
		socket.on('message', data=>{
			console.log(`${socket.id} ${socket.name}: ${data}`)
			socket.broadcast.emit('message', {name: socket.name, message: data})
		})
	})

	//start
	const port = process.env.PORT || 80
	server.listen(port)
	log.info(`Listening on ${port}`)
})
