import {writable} from 'svelte/store'
const cache = new Map()
export default (key, defaultValue='', parse=d=>d)=>{
	if(cache.has(key)) return cache.get(key) //prevents duplicate accessors to local storage
	const string = typeof defaultValue === 'string' //localSTorage only stores strings, so if needs conversion

	const stored = localStorage.getItem(key)
	const variable = writable(parse(string? stored: JSON.parse(stored)))
	variable.subscribe(string?
		v=> localStorage.setItem(key, v):
		v=> localStorage.setItem(key, JSON.stringify(v))
	)

	if(stored===null) variable.set(defaultValue) //set default if DNE
	cache.set(key, variable) //synchronize to cache
	return variable
}
