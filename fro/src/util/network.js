const relURL = url=>{
	if(process.env.SERVER){
		const relative = new URL(document.baseURI).origin === new URL(url, document.baseURI).origin
		if(relative) url = (new URL(url, process.env.SERVER)).toString()
	}
	return url
}

/*
Enhanced fetch
- relativizes url on process.env.SERVER
- forwards data the right way automatically, based on Content-Type header
*/
const fetch2 = (url, opts={})=>{
	let callingURL = relURL(url)
	if(url !== callingURL) opts.mode = 'cors'

	if(!opts.method || opts.method === 'GET'){
		if(opts.data) callingURL += '?' + (new URLSearchParams(opts.data)).toString()
	}else if(opts.headers){
		switch(opts.headers['Content-Type']){
			case 'application/x-www-form-urlencoded':
				if(opts.data) opts.body = (new URLSearchParams(opts.data)).toString()
				break;
			case 'application/json':
				if(opts.data) opts.body = JSON.stringify(opts.data)
				break;
		}
	}
	return fetch(callingURL, opts)
}


module.exports = {
	relURL,
	fetch: fetch2
}
